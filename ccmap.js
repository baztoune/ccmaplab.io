import strava from './strava.js';
import panel from './panel.js';

const map = L.map('map');

const DEFAULT_BOUNDS = L.latLngBounds([
  [49.898, 3.96],
  [46.982, -0.705],
]);

map.fitBounds(DEFAULT_BOUNDS);

// const TILES_PATTERN = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
// const ATTRIBUTION = "© <a href='https://www.openstreetmap.org'>OpenStreetMap</a> contributors";
const TILES_PATTERN = "https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}.png";
const ATTRIBUTION = "© <a href='https://www.openstreetmap.org'>OpenStreetMap</a> contributors, © <a href='https://carto.com/attribution'>CARTO</a>";
L.tileLayer(
  TILES_PATTERN, {
  maxZoom: 18,
  attribution: ATTRIBUTION,
}).addTo(map);

let pUserPolylines;
function getUserPolylines() {
  if (!pUserPolylines) {
    pUserPolylines = strava.hasStravaAccess() ?
      strava.getRidesPolylines() : Promise.resolve([]);
  }
  return pUserPolylines;
}

// FIXME: this function is only usefull to fill cc-list.json with
// distance, elevation and summary_polyline (with copy/paste).
// It should be part of some compilation step.
function fillChallengesData() {
  fetch("cc-list.json").then(response => response.json())
    .then(challenges => Promise.all(
      challenges.map(challenge => strava.getRoute(challenge.strava_route_id)
        .then((route) =>
          Object.assign(challenge, {
            distance: route.distance,
            elevation: route.elevation_gain,
            summary_polyline: route.map.summary_polyline
          })))))
    .then(JSON.stringify)
    .then(console.log);
}
// fillChallengesData();

function getChallenges() {
  return fetch("cc-list.json").then(response => response.json());
}

const finishIcon = L.icon({
  iconUrl: 'finish.svg',
  iconSize: [15, 15],
});

let pAllChallenges;
function getChallengesWithCoverage() {
  if (!pAllChallenges) {
    const pMathWorker = getUserPolylines().then(getMathWorker);
    pAllChallenges = Promise.all([pMathWorker, getChallenges()])
      .then(([mathWorker, challenges]) => Promise.all(
        challenges.map(challenge => fillChallengeCoverage(challenge, mathWorker))));
  }
  return pAllChallenges;
}

function fillChallengeCoverage(challenge, mathWorker) {
  const clickHandler = (event) => {
    L.DomEvent.stopPropagation(event);
    focusChallenge(challenge);
  }

  const trackPoints = L.PolylineUtil.decode(challenge.summary_polyline);
  return classifySegmentsCoverage(trackPoints, mathWorker)
    .then(coverage => {
      const uncoveredLayers = coverage.uncoveredSegments.map(segment =>
        L.polyline(segment, UNCOVERED_SEGMENT_STYLE));
      const coveredLayers = coverage.coveredSegments.map(segment =>
        L.polyline(segment, COVERED_SEGMENT_STYLE));
      const polylinesGroup = L.featureGroup(uncoveredLayers.concat(coveredLayers));
      polylinesGroup.on("click", clickHandler);
      const finishMarker = L.marker(trackPoints[trackPoints.length - 1], { icon: finishIcon });
      finishMarker.on("click", clickHandler);

      const featureGroup = L.featureGroup([polylinesGroup, finishMarker]);

      return Object.assign(challenge, coverage, { featureGroup, polylinesGroup });
    });
}

function getMathWorker(userPolylines) {
  return new Promise((resolve) => {
    const mathWorker = new Worker("math-worker.js");
    function initEventHandler() {
      mathWorker.removeEventListener("message", initEventHandler);
      resolve(mathWorker);
    }
    mathWorker.addEventListener("message", initEventHandler);
    mathWorker.postMessage({
      func: 'loadUserPolylines',
      polylines: userPolylines,
    });
  });
}

function classifySegmentsCoverage(trackPoints, mathWorker) {
  return new Promise(resolve => {
    const id = Math.random();
    function eventHandler(event) {
      if (event.data.id === id) {
        mathWorker.removeEventListener("message", eventHandler);
        resolve(event.data.result);
      }
    }
    mathWorker.addEventListener("message", eventHandler);
    mathWorker.postMessage({
      func: 'classifySegmentsCoverage',
      id,
      trackPoints,
    });
  });
}

let pUserTracesFeatureGroup;
function drawUserTraces() {
  if (panel.userTracesEnabled()) {
    if (!pUserTracesFeatureGroup) {
      pUserTracesFeatureGroup = getUserPolylines().then(polylines =>
        L.featureGroup(polylines.map(p => L.polyline(p, USER_TRACE_STYLE))));
    }
    return pUserTracesFeatureGroup.then(featureGroup => featureGroup.addTo(map));
  }
  if (pUserTracesFeatureGroup) {
    return pUserTracesFeatureGroup.then(featureGroup => featureGroup.remove());
  }
  return Promise.resolve();
}

function drawChallengesCoverage() {
  return getChallengesWithCoverage()
    .then(challenges => {
      challenges.forEach(challenge => challenge.featureGroup.remove());
      const displayedChallenges = challenges.filter(panel.matchFilter);
      displayedChallenges.forEach(challenge => {
        challenge.featureGroup.addTo(map);
      });
      panel.showChallengesDetails(displayedChallenges);
      forceBounds();
    });
}

const DEFAULT_PATH_OPTIONS = {
  renderer: L.canvas({
    padding: 0.5,
    tolerance: 10,
  }),
};
const USER_TRACE_STYLE = {
  color: '#3388ff',
  weight: 2,
  opacity: 0.75,
  ...DEFAULT_PATH_OPTIONS,
}
const UNFOCUSED_SEGMENT_STYLE = {
  weight: 2,
  opacity: 0.5,
  ...DEFAULT_PATH_OPTIONS,
};
const FOCUSED_SEGMENT_STYLE = {
  weight: 4,
  opacity: 1,
  ...DEFAULT_PATH_OPTIONS,
};
const COVERED_SEGMENT_STYLE = {
  color: '#3388ff',
  ...UNFOCUSED_SEGMENT_STYLE,
};
const UNCOVERED_SEGMENT_STYLE = {
  color: '#ee6352',
  ...UNFOCUSED_SEGMENT_STYLE,
};

let focusedChallenge;
function focusChallenge(challenge) {
  if (challenge === focusedChallenge) {
    return Promise.resolve();
  }
  focusedChallenge = challenge;
  return getChallengesWithCoverage().then(allChallenges => {
    allChallenges.forEach(c => {
      const style = c === challenge ? FOCUSED_SEGMENT_STYLE : UNFOCUSED_SEGMENT_STYLE;
      c.featureGroup.setStyle(style);
    });
    forceBounds();
    panel.showChallengeDetails(challenge);
  });
}
map.on("click", () => focusChallenge(null));

function forceBounds() {
  let pBounds;
  if (focusedChallenge) {
    pBounds = Promise.resolve(focusedChallenge.polylinesGroup.getBounds());
  } else {
    pBounds = getChallengesWithCoverage().then(allChallenges => {
      const displayedChallenges = allChallenges.filter(panel.matchFilter);
      if (displayedChallenges.length === 0) {
        return DEFAULT_BOUNDS;
      }
      return displayedChallenges
        .map(c => c.polylinesGroup.getBounds())
        .reduce((acc, bounds) => acc.extend(bounds));
    });
  }
  return pBounds.then(bounds => map.fitBounds(bounds));
}

panel.onFilterChanged(() => {
  withLoading(drawChallengesCoverage());
  if (focusedChallenge && !panel.matchFilter(focusedChallenge)) {
    focusChallenge(null);
  }
});

panel.onShowUserTracesChanged(() => withLoading(drawUserTraces()));

function withLoading(pTask) {
  const loadingElt = document.getElementById("loading");
  loadingElt.style.display = "block";
  return pTask.finally(() => loadingElt.style.display = "none");
}

withLoading(Promise.all([
  drawChallengesCoverage(),
  drawUserTraces(),
]));
